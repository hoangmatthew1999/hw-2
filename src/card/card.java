package card;

import java.util.Scanner;
import java.util.HashMap; // import the HashMap class

public class card {
    public String input;
    card(){
        String loopExitCase = "in loop";
        do {
            Scanner myObj = new Scanner(System.in);
            System.out.println("Enter the card notation");
            input = myObj.nextLine();
            if(input.length() == 2 || input.length() == 3){// to me its more readible than to say if its greater than 1 and less than 3
                System.out.println(" ");

            }
            else{
                System.out.println("invalid length");
                continue;
            }


            if(testingInput(input) == false){
                System.out.println("the values are not correct");
                continue;
            }
            loopExitCase = "Exiting loop";
        }while (loopExitCase == "in loop");
    }
    public String getDescription(String argument){
        /*
        instead of making a whole bunch of switch statements, i created a lookup table and lookuped the data
        */

        HashMap<String, String> value = new HashMap<String, String>();
        value.put("A","Ace");
        value.put("2","Two");
        value.put("3","Three");
        value.put("4","Four");
        value.put("5","Five");
        value.put("6","Six");
        value.put("7","Seven");
        value.put("8","Eight");
        value.put("9","Nine");
        value.put("10","Ten");
        value.put("J","Jack");
        value.put("Q","Queen");
        value.put("K","King");

        HashMap<String, String> suit = new HashMap<String, String>();
        suit.put("S","Spade");
        suit.put("C","Clover");
        suit.put("D","Diamond");
        suit.put("H","Heart");

        String returnString = "";

        String valueToString = Character.toString( argument.charAt(0)).toUpperCase();

        String returnValue = value.get(valueToString);

        String suitToString = Character.toString(argument.charAt(1)).toUpperCase();
        String returnSuit = suit.get(suitToString);

        returnString = returnValue + " of " + returnSuit;
        return returnString;
    }
    public static boolean testingInput(String argument){
        /*
        instead of making a whole bunch of switch statements, i created a lookup table and lookuped the data
        */

        HashMap<String, String> value = new HashMap<String, String>();
        value.put("A","Ace");
        value.put("2","Two");
        value.put("3","Three");
        value.put("4","Four");
        value.put("5","Five");
        value.put("6","Six");
        value.put("7","Seven");
        value.put("8","Eight");
        value.put("9","Nine");
        value.put("10","Ten");
        value.put("J","Jack");
        value.put("Q","Queen");
        value.put("K","King");

        HashMap<String, String> suit = new HashMap<String, String>();
        suit.put("S","Spade");
        suit.put("C","Clover");
        suit.put("D","Diamond");
        suit.put("H","Heart");


        if(argument.length() == 3) {
            String returnValue = "";
            String ifTen = argument.substring(0, 2);
            System.out.println(ifTen);
            returnValue = "10";//already checked if it is 10 in the input
            String suitToString = Character.toString(argument.charAt(2)).toUpperCase();
            String returnSuit = suit.get(suitToString);

        }

        String returnString = "";
        String valueToString = Character.toString( argument.charAt(0) ).toUpperCase();

        String returnValue = value.get(valueToString);

        String suitToString = Character.toString(argument.charAt(1)).toUpperCase();
        String returnSuit = suit.get(suitToString);

        returnString = returnValue + " of " + returnSuit;
        if (returnString == null){return false;}
        if (returnValue == null){return false;}
        return true;
    }


    public static void main(String[] args) {
        card instance = new card();
        System.out.println(instance.input);
        System.out.println( instance.getDescription(instance.input) );
    }
}
